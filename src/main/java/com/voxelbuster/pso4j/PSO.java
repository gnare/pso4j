/*
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.voxelbuster.pso4j;/*
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.nd4j.common.primitives.Quad;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.conditions.Conditions;
import org.nd4j.linalg.ops.transforms.Transforms;
import org.nd4j.linalg.string.NDArrayStrings;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.IntStream;

/**
 * The {@code com.voxelbuster.pso4j.PSO} class implements the Particle Swarm Optimization algorithm,
 * allowing for the optimization of an objective function subject to constraints.
 * <p>
 * See  <a href="https://github.com/tisimst/pyswarm/blob/master/pyswarm/pso.py">the reference pyswarm implementation</a> for more info.
 */
@SuppressWarnings("SpellCheckingInspection")
@Slf4j
public class PSO {

    /**
     * Wraps the objective function to be optimized.
     *
     * @param func The function to be optimized.
     * @param x The input parameters for the function as an INDArray.
     * @return The computed objective function value as a double.
     */
    public static double objectiveWrapper(Function<INDArray, Double> func, INDArray x) {
        return func.apply(x);
    }

    /**
     * Wraps the feasibility check function to determine if a given solution is feasible.
     *
     * @param func The function that determines the feasibility of a solution.
     * @param x The input parameters for the function as an INDArray.
     * @return An INDArray of 0s and 1s indicating the feasibility of each constraint.
     */
    public static INDArray isFeasibleWrapper(Function<INDArray, INDArray> func, INDArray x) {
        return func.apply(x).cond(Conditions.greaterThanOrEqual(0));
    }

    /**
     * Provides a wrapper for the absence of constraints on the solution.
     *
     * @param x The input parameters for the function as an INDArray.
     * @return An INDArray with a single element set to 0, indicating no constraints.
     */
    public static INDArray constraintNoneWrapper(INDArray x) {
        return Nd4j.createFromArray(0);
    }

    /**
     * Wraps inequality constraints into a single function.
     *
     * @param ieqcons A list of functions representing inequality constraints.
     * @param x The input parameters for the functions as an INDArray.
     * @return An INDArray indicating the result of each inequality constraint.
     */
    public static INDArray constraintIeqconsWrapper(List<Function<INDArray, INDArray>> ieqcons, INDArray x) {
        return Nd4j.create(ieqcons.stream()
            .map(y -> y.apply(x))
            .toList(), x.shapeDescriptor().getShape());
    }

    /**
     * Wraps a function that provides inequality constraints.
     *
     * @param fIeqcons The function returning an array of doubles representing inequality constraints.
     * @param x The input parameters for the function as an INDArray.
     * @return An INDArray representing the inequality constraints.
     */
    public static INDArray constraintFIeqconsWrapper(Function<INDArray, double[]> fIeqcons, INDArray x) {
        return Nd4j.create(fIeqcons.apply(x));
    }

    /**
     * Executes the Particle Swarm Optimization algorithm with the given parameters.
     *
     * @param par The parameters for the com.voxelbuster.pso4j.PSO algorithm encapsulated in a {@code Params} object.
     * @return A {@code Quad} containing the best position found, the value of the objective function at that position,
     *         and optionally the positions and objective function values of all particles if {@code particleOutput} is true.
     */
    @SuppressWarnings({"resource", "DuplicatedCode"})
    public static Quad<INDArray, Double, INDArray, INDArray> runPSO(@NonNull Params par) {
        if (par.lowerBound.length != par.upperBound.length) {
            throw new RuntimeException("Lower- and upper-bounds must be the same length");
        }

        INDArray lb = Nd4j.create(par.lowerBound);
        INDArray ub = Nd4j.create(par.upperBound);


        if (!ub.gt(lb).all()) {
            throw new RuntimeException("All upper-bound values must be greater than lower-bound values");
        }


        INDArray vhigh = Transforms.abs(ub.sub(lb));
        INDArray vlow = vhigh.mul(-1);

        Function<INDArray, INDArray> constraintFunc;

        Function<INDArray, Double> objectiveFunc = (x) -> objectiveWrapper(par.func, x);

        final ExecutorService pool;

        // Check constraint functions
        if (par.fIeqcons == null) {
            if (par.ieqcons.isEmpty()) {
                if (par.debug) {
                    log.info("No constraints given.");
                }

                constraintFunc = PSO::constraintNoneWrapper;
            } else {
                if (par.debug) {
                    log.info("Converting ieqcons to a single constraint function");
                }

                constraintFunc = (x) -> constraintIeqconsWrapper(par.ieqcons, x);
            }
        } else {
            if (par.debug) {
                log.info("Single constraint function given in fIeqcons");
            }
            constraintFunc = (x) -> constraintFIeqconsWrapper(par.fIeqcons, x);
        }

        Function<INDArray, INDArray> isFeasible = (x) -> isFeasibleWrapper(constraintFunc, x);

        if (par.multithreaded) {
            pool = Executors.newCachedThreadPool();
        } else {
            pool = null;
        }

        // Initialize swarm
        int S = par.swarmSize;
        int D = (int) lb.length();
        INDArray x = Nd4j.rand(S, D);

        @SuppressWarnings("UnusedAssignment")
        INDArray v = Nd4j.zerosLike(x);

        INDArray p = Nd4j.zerosLike(x);
        INDArray fx = Nd4j.zeros(S);
        INDArray fs = Nd4j.zeros(DataType.BOOL, S);
        INDArray fp = Nd4j.ones(S).mul(Double.POSITIVE_INFINITY);
        INDArray g = Nd4j.zerosLike(x.getRow(0));
        double fg = Double.POSITIVE_INFINITY;

        // Initialize particle positions
        x = lb.add(x.mul(ub.sub(lb)));

        // Create objectives and constraints
        final INDArray finalX = Nd4j.create(x.shape());
        Nd4j.copy(x, finalX);
        if (par.multithreaded) {
            Objects.requireNonNull(pool);
            IntStream.range(0, S)
                .mapToObj(i ->
                    pool.submit(() -> {
                        INDArray row = finalX.getRow(i);
                        fx.putScalar(i, objectiveFunc.apply(row));
                        fs.put(i, isFeasible.apply(row));
                    })
                ).toList().forEach(future -> { // Wait for tasks to complete
                    try {
                        future.get();
                    } catch (InterruptedException | ExecutionException e) {
                        throw new RuntimeException(e);
                    }
                });
        } else {
            IntStream.range(0, S)
                .forEach(i -> {
                    INDArray row = finalX.getRow(i);
                    fx.putScalar(i, objectiveFunc.apply(row));
                    fs.put(i, isFeasible.apply(row));
                });
        }

        // Store particle's best position
        INDArray iUpdate = Transforms.and(fx.lt(fp), fs);
        final INDArray finalX1 = Nd4j.create(x.shape());
        Nd4j.copy(x, finalX1);
        IntStream.range(0, S)
            .filter(i -> iUpdate.getInt(i) == 1)
            .forEach(i -> {
                p.getRow(i).assign(finalX1.getRow(i));
                fp.putScalar(i, fx.getDouble(i));
            });


        // Update swarm best postion
        INDArray iMin = Nd4j.argMin(fp);
        if (fp.get(iMin).getDouble(0) < fg) {
            fg = fp.get(iMin).getDouble(0);
            Nd4j.copy(p.getRow(iMin.getLong(0)), g);
        } else {
            Nd4j.copy(x.getRow(0), g);
        }

        // Initialize the particle's velocity
        v = vlow.add(Nd4j.rand(S, D).mul(vhigh.sub(vlow)));

        // Iterate until termination criterion met
        INDArray pMin = Nd4j.zerosLike(p.getRow(0));
        for (int it = 1; it < par.maxIter; it++) {
            INDArray rp = Nd4j.rand(S, D);
            INDArray rg = Nd4j.rand(S, D);

            // Update particles' velocities
            v = v.mul(par.omega)
                .add(rp.mul(par.phiP).mul(p.sub(x)))
                .add(rg.mul(par.phiG).mul(g.sub(x)));

            // Update positions
            x = x.add(v);

            INDArray maskL = x.lt(lb);
            INDArray maskU = x.gt(ub);

            x = x.mul(Transforms.not(Transforms.or(maskL, maskU)))
                .add(lb.mul(maskL))
                .add(ub.mul(maskU));

            // Update objectives and constraints
            final INDArray finalX2 = Nd4j.create(x.shape());
            Nd4j.copy(x, finalX2);
            if (par.multithreaded) {
                Objects.requireNonNull(pool);
                IntStream.range(0, S)
                    .mapToObj(i ->
                        pool.submit(() -> {
                            INDArray row = finalX2.getRow(i);
                            fx.putScalar(i, objectiveFunc.apply(row));
                            fs.put(i, isFeasible.apply(row));
                        })
                    ).toList().forEach(future -> { // Wait for tasks to complete
                        try {
                            future.get();
                        } catch (InterruptedException | ExecutionException e) {
                            throw new RuntimeException(e);
                        }
                    });
            } else {
                IntStream.range(0, S)
                    .forEach(i -> {
                        INDArray row = finalX2.getRow(i);
                        fx.putScalar(i, objectiveFunc.apply(row));
                        fs.put(i, isFeasible.apply(row));
                    });
            }

            // Store particle's best position
            INDArray iUpdate2 = Transforms.and(fx.lt(fp), fs);
            final INDArray finalX4 = Nd4j.create(x.shape());
            Nd4j.copy(x, finalX4);
            IntStream.range(0, S)
                .filter(i -> iUpdate2.getInt(i) == 1)
                .forEach(i -> {
                    p.getRow(i).assign(finalX4.getRow(i));
                    fp.putScalar(i, fx.getDouble(i));
                });

            // Compare swarm positions
            iMin = Nd4j.argMin(fp);
            if (fp.get(iMin).getDouble(0) < fg) {
                if (par.debug) {
                    log.info("New best for swarm at iteration {} {} {}",
                        it,
                        p.getRow(iMin.getLong(0)).toString(new NDArrayStrings(4)),
                        fp.get(iMin).toString(new NDArrayStrings(4))
                    );
                }

                Nd4j.copy(p.getRow(iMin.getLong(0)), pMin);

                INDArray stepSize = Transforms.sqrt(Transforms.pow(g.sub(pMin), 2).sum());

                if (Transforms.abs(fp.get(iMin).mul(-1).add(fg)).lte(par.minFunc).all()) {
                    log.info("Stopping search: Swarm best objective change less than {}", par.minFunc);
                    if (par.particleOutput) {
                        return new Quad<>(pMin, fp.get(iMin).getDouble(0), p, fp);
                    } else {
                        return new Quad<>(pMin, fp.get(iMin).getDouble(0), null, null);
                    }
                } else if (stepSize.lte(par.minStep).all()) {
                    log.info("Stopping search: Swarm best position change less than {}", par.minStep);
                    if (par.particleOutput) {
                        return new Quad<>(pMin, fp.get(iMin).getDouble(0), p, fp);
                    } else {
                        return new Quad<>(pMin, fp.get(iMin).getDouble(0), null, null);
                    }
                } else {
                    Nd4j.copy(pMin, g);
                    fg = fp.get(iMin).getDouble(0);
                }
            }

            if (par.debug) {
                log.info("Best after iteration {} {} {}",
                    it, g.toString(new NDArrayStrings(4)), fg);
            }
        }

        log.info("Stopping search: maximum iterations reached --> {}", par.maxIter);

        if(isFeasible.apply(g).all()) {
            log.error("com.voxelbuster.pso4j.PSO couldn't find a feasible design.");
        }

        if (par.particleOutput) {
            return new Quad<>(g, fg, p, fp);
        } else {
            return new Quad<>(g, fg, null, null);
        }
    }

    /**
     * This {@code Params} class holds the parameters for the {@code com.voxelbuster.pso4j.PSO.runPSO()} algorithm. <br />
     * Only the func, lowerBound, and upperBound parameters are required.
     */
    @Data
    public static class Params {
        private final Function<INDArray, Double> func;
        private final double[] lowerBound;
        private final double[] upperBound;

        private List<Function<INDArray, INDArray>> ieqcons = List.of();
        private Function<INDArray, double[]> fIeqcons = null;

        private int swarmSize = 100;
        private double omega = 0.5;
        private double phiP = 0.5;
        private double phiG = 0.5;
        private int maxIter = 100;

        private double minStep = 1E-8;
        private double minFunc = 1E-8;

        private boolean debug = false ;
        private boolean multithreaded = false;
        private boolean particleOutput = false;

    }

}
