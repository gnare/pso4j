/*
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.voxelbuster.pso4j;import com.voxelbuster.pso4j.PSO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.nd4j.common.primitives.Quad;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.string.NDArrayStrings;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.lang.Math.pow;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class PSOTest {

    @Test
    public void test4thOrderBananaFunc() {
        log.info("Example minimization of 4th-order banana function (no constraints)");
        Function<INDArray, Double> objective = (x) -> {
            double x1 = x.getDouble(0);
            double x2 = x.getDouble(1);
            return pow(x1, 4) - pow(x1, 2) * x2 * 2 + pow(x2, 2) + pow(x1, 2) - x1 * 2 + 5;
        };

        PSO.Params params = new PSO.Params(objective, new double[]{-3, -1}, new double[]{2, 6});
        Quad<INDArray, Double, INDArray, INDArray> out = PSO.runPSO(params);
        INDArray xopt1 = out.getFirst();
        double fopt1 = out.getSecond();

        assertEquals(1, xopt1.getDouble(0), 0.01);
        assertEquals(1, xopt1.getDouble(1), 0.01);
        assertEquals(4, fopt1, 0.01);

        log.info("Optimum is at: {}", xopt1.toString(new NDArrayStrings(4)));
        log.info("Optimal function value: {}", fopt1);
    }

    @Test
    public void testStatisticallySignificant() {
        AtomicInteger complete = new AtomicInteger(0);
        ExecutorService pool = Executors.newCachedThreadPool();
        IntStream.range(0, 50).mapToObj(i -> pool.submit(this::test4thOrderBananaFunc)
        ).toList().forEach(future -> {
            try {
                future.get();
                log.info(String.valueOf(complete.addAndGet(1)));
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @SuppressWarnings("ExtractMethodRecommender")
    @Test
    public void test4thOrderBananaFuncWithConstraint() {
        log.info("Example minimization of 4th-order banana function (with constraint)");
        Function<INDArray, Double> objective = (x) -> {
            double x1 = x.getDouble(0);
            double x2 = x.getDouble(1);
            return pow(x1, 4) - pow(x1, 2) * x2 * 2 + pow(x2, 2) + pow(x1, 2) - x1 * 2 + 5;
        };

        Function<INDArray, double[]> constraint = (x) -> {
            double x1 = x.getDouble(0);
            double x2 = x.getDouble(1);
            return new double[]{-pow((x1 + 0.25), 2) + 0.75 * x2};
        };

        PSO.Params params = new PSO.Params(objective, new double[]{-3, -1}, new double[]{2, 6});
        params.setFIeqcons(constraint);
        Quad<INDArray, Double, INDArray, INDArray> out = PSO.runPSO(params);
        INDArray xopt1 = out.getFirst();
        double fopt1 = out.getSecond();

        assertEquals(0.5, xopt1.getDouble(0), 0.05);
        assertEquals(0.75, xopt1.getDouble(1), 0.01);
        assertEquals(4.5, fopt1, 0.01);

        log.info("Optimum is at: {}", xopt1.toString(new NDArrayStrings(4)));
        log.info("Optimal function value: {}", fopt1);
    }
}
